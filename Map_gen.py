import numpy as np
from matplotlib.colors import LightSource
import matplotlib.pyplot as plt
import perlin
import Settings as s
from PIL import Image, ImageFilter

sn = perlin.SimplexNoise()

def Map1(sea_level, oct1, oct2, oct3, oct4, oct5, oct6):
	s.DEM = np.zeros((s.w, s.h), dtype=np.uint8)

	sn.randomize()

	def Noise(x, y):
			octave1 = int(( sn.noise2(x*oct1, y*oct1) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave2 = int(( sn.noise2(x*oct2, y*oct2) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave3 = int(( sn.noise2(x*oct3, y*oct3) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave4 = int(( sn.noise2(x*oct4, y*oct4) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave5 = int(( sn.noise2(x*oct5, y*oct5) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			octave6 = int(( sn.noise2(x*oct6, y*oct6) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
			noise = (octave1 + octave2 + octave3/2 + octave4/10 + octave5/6 + octave6/6)
			return noise

	coords = np.argwhere(s.DEM == 0)

	for x,y in coords:
		s.DEM[x,y] = Noise(x + s.dX, y + s.dY)

	Tile = np.zeros((s.w, s.h, 3), dtype=np.uint8)
	ls = LightSource(azdeg=315, altdeg=45)

	for x,y in coords:
		if s.DEM[x,y] <= sea_level:
			Tile[x,y] = [0,0,s.DEM[x,y]]
		elif s.DEM[x,y] <= sea_level + 20:
			Tile[x,y] = [s.DEM[x,y],s.DEM[x,y],s.DEM[x,y]/2]
		elif s.DEM[x,y] >= s.mountain_level:
			Tile[x,y] = [s.DEM[x,y]/2,s.DEM[x,y]/4,0]
		else:
			Tile[x,y] = [0,s.DEM[x,y]/2,0]

	s.DEM2 = s.DEM
	s.DEM = s.DEM.astype('float')
	s.DEM[s.DEM <= s.mountain_level-50] = 'nan'

	plt.imshow(ls.hillshade(s.DEM, vert_exag=0.5), cmap='gray')
	plt.axis('off')
	plt.savefig('DEM.png', bbox_inches='tight', pad_inches = 0)
	plt.clf()

	plt.imshow(Tile)
	plt.axis('off')
	plt.savefig('Tile.png', bbox_inches='tight', pad_inches = 0)
	plt.clf()

	tile = Image.open('Tile.png')
	tile = tile.resize((1000, 1000), Image.NEAREST)
	dem = Image.open('DEM.png')
	dem = dem.resize((1000, 1000), Image.NEAREST)


	blend = Image.blend(tile, dem, 0.2)

	tile.save("Map2.gif")
