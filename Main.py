from Tkinter import *
import Map_gen as Build
import Settings as s
import Game

root = Tk()
root.title("Map Generator")

def Next():
	Game.Play(root, sealevel.get(), oct1Scale.get(), oct2Scale.get(), oct3Scale.get(), oct4Scale.get(), oct5Scale.get(), oct6Scale.get())

def Generate():
    w.delete(ALL)
    s.seed = seedentry.get()
    Build.Map1(sealevel.get(),oct1Scale.get(),oct2Scale.get(),oct3Scale.get(),oct4Scale.get(),oct5Scale.get(),oct6Scale.get())
    im = PhotoImage(file='Map2.gif')
    im = im
    m = w.create_image(-50,0,anchor=NW,image=im)
    w.image = im

sealevel_label = Label(root, text="Sea Level").grid(column = 0, row= 0)
sealevel = Scale(root, from_= 0, to=255, orient = HORIZONTAL)
sealevel.set(100)
sealevel.grid(column=1,row=0)

oct1level_label = Label(root, text="Octave 1").grid(column = 0, row= 1)
oct1Scale = Scale(root, from_= 0.0, to=0.02, orient = HORIZONTAL, resolution = 0.001)
oct1Scale.set(0.015625)
oct1Scale.grid(column=1,row=1)

oct2level_label = Label(root, text="Octave 2").grid(column = 0, row= 2)
oct2Scale = Scale(root, from_= 0.02, to=0.04, orient = HORIZONTAL, resolution = 0.001)
oct2Scale.set(0.03125)
oct2Scale.grid(column=1,row=2)

oct3level_label = Label(root, text="Octave 3").grid(column = 0, row= 3)
oct3Scale = Scale(root, from_= 0.04, to=0.1, orient = HORIZONTAL, resolution = 0.01)
oct3Scale.set(0.0625)
oct3Scale.grid(column=1,row=3)

oct4level_label = Label(root, text="Octave 4").grid(column = 0, row= 4)
oct4Scale = Scale(root, from_= 0.1, to=0.2, orient = HORIZONTAL, resolution = 0.01)
oct4Scale.set(0.125)
oct4Scale.grid(column=1,row=4)

oct5level_label = Label(root, text="Octave 5").grid(column = 0, row= 5)
oct5Scale = Scale(root, from_= 0.2, to=0.3, orient = HORIZONTAL, resolution = 0.01)
oct5Scale.set(0.25)
oct5Scale.grid(column=1,row=5)

oct6level_label = Label(root, text="Octave 6").grid(column = 0, row= 6)
oct6Scale = Scale(root, from_= 0.5, to=1.0, orient = HORIZONTAL, resolution = 0.1)
oct6Scale.set(0.8)
oct6Scale.grid(column=1,row=6)

v = IntVar()
v.set('')
seedlabel = Label(root, text="Enter Seed:").grid(column = 0, row = 7)
seedentry = Entry(root, textvariable= v)
seedentry.grid(column=1, row=7)

next = Button(root, text="Next", command= lambda: Next())
next.grid(column=1, row=8)

b = Button(root, text="Generate", command= lambda: Generate())
b.grid(column=0,row=8)

w = Canvas(root, width=600, height=600)
w.grid(column=2,row=0,rowspan = 9)


root.mainloop()