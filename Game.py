from Tkinter import *
import Settings as s
import Map_gen as Build
import Line_gen as Line
import numpy as np
from PIL import Image

def Play(root, sea_level, oct1, oct2, oct3, oct4, oct5, oct6):
	for widget in root.winfo_children():
   		widget.destroy()


   	def Refresh():
   		print np.asarray((Build.Map1(sea_level, oct1, oct2, oct3, oct4, oct5, oct6)))

   	def motion(event):
		s.x, s.y = event.x, event.y
		if s.x <= 20 and s.Xoff <= -55:
			s.dX = s.dX + 15
			s.Xoff = s.Xoff + 15
		if s.x >= 580 and s.Xoff >= -390:
			s.dX = s.dX - 15
			s.Xoff = s.Xoff - 15
		if s.y <= 20 and s.Yoff <= -15:
			s.dY = s.dY + 15
			s.Yoff = s.Yoff + 15
		if s.y >= 580 and s.Yoff >= -345:
			s.dY = s.dY - 15
			s.Yoff = s.Yoff - 15
		Play(root, sea_level, oct1, oct2, oct3, oct4, oct5, oct6)

	def QueryLocation(x, y):
		height = s.DEM[(s.dY-(s.dY+10))/10, (s.dX - (s.dX+50))/10]
		if height >= 100:
			return 'Land'
		else:
			return "Ocean"


	b = Button(root, text='options')
	b.grid(column=0,row=1)

	coords = Label(root, text='x: %d y: %d'%((s.x-(s.Xoff+50))/10, (s.y-(s.Yoff+10))/10))
	coords.grid(column=0, row=3)

	top_bar = Button(root, text='topbar')
	top_bar.grid(column=1,row=0)

	x = Canvas(root, width=600, height=600)
	x.grid(column=1,row=1)





	im = PhotoImage(file='Map2.gif')
	im = im
	x.create_image(s.Xoff,s.Yoff,anchor=NW,image=im)
	x.im = im

	x.bind('<Motion>', motion)

