import numpy as np
from matplotlib.colors import LightSource
import matplotlib.pyplot as plt
import perlin
import Settings as s
from PIL import Image, ImageFilter

sn = perlin.SimplexNoise()

def Noise(x, y, sea_level, oct1, oct2, oct3, oct4, oct5, oct6):
	octave1 = int(( sn.noise2(x*oct1, y*oct1) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
	octave2 = int(( sn.noise2(x*oct2, y*oct2) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
	octave3 = int(( sn.noise2(x*oct3, y*oct3) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
	octave4 = int(( sn.noise2(x*oct4, y*oct4) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
	octave5 = int(( sn.noise2(x*oct5, y*oct5) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
	octave6 = int(( sn.noise2(x*oct6, y*oct6) - -1.90444436692 ) * 255 / ( 1.93171936666 - -1.90444436692 ) + 0)
	noise = (octave1 + octave2 + octave3/2 + octave4/10 + octave5/6 + octave6/6)
	return noise

def Build(sea_level, oct1, oct2, oct3, oct4, oct5, oct6):
	n = -1
	line = np.zeros((100), dtype=np.uint8)

	for pixel in line:
		s.n = s.n + 1
		line[s.n] = Noise(s.n + s.dX, 99 + s.dY, sea_level, oct1, oct2, oct3, oct4, oct5, oct6)

	for x in line:
		n = n + 1
		if x <= sea_level:
			line[n] = [0,0,x]
		elif x <= sea_level + 20:
			line[n] = [x,x,x/2]
		elif x >= s.mountain_level:
			line[n] = [x/2,x/4,0]
		else:
			line[n] = [0,x/2,0]

	tile = np.vstack((s.DEM2, line))

	tile = np.delete(tile, (0), axis=0)

	plt.imshow(tile)
	plt.axis('off')
	plt.savefig('Tile.png', bbox_inches='tight', pad_inches = 0)
	plt.clf()

	tile = Image.open('Tile.png')
	tile = tile.resize((1000, 1000), Image.NEAREST)
	tile = tile.filter(ImageFilter.GaussianBlur(radius=1.8))
	tile.save("Map2.gif")